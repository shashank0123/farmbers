import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,
    FlatList,
    Modal,
    I18nManager,
    RefreshControl,
    fetchData, SafeAreaView, ImageBackground
} from "react-native";
const { width, height } = Dimensions.get('window');
const scalesPageToFit = Platform.OS === 'android';
import { Images } from '../../utils';
import styles from './style';
import { WebView } from 'react-native-webview';


class DashboardMap extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.upperBar}>
                    <View style={styles.logoContainer}>
                        <Image source={Images.farmLogo} style={styles.logoStyle} />
                    </View>
                    <View style={{ position: 'absolute', left: '70%', flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('NotificationScreen')}>
                            <Image source={Images.bell} style={styles.bellIcon1} />
                        </TouchableOpacity>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={Images.profilePic} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginLeft: 8 }}>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('ReferAndEarn')}>
                                <Image source={Images.refer} style={styles.bellIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.wrapper}>
                    <View style={styles.upperWrapper}>
                        <Text style={{ fontSize: 24, marginTop: 10, color: '#fff' }}>Dashboard</Text>
                    </View>
                </View>


                {/* <WebView
                    scalesPageToFit={scalesPageToFit}
                    bounces={false}
                    scrollEnabled={false}
                    // source={{ uri: 'https://netahub.com/terms-and-conditions/' }}
                    source={{
                        html: `
                              <!DOCTYPE html>
                              <html>
                                <head></head> 
                                <body>
                                  <div id="baseDiv"> <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14005.718839135889!2d77.34392775!3d28.64684955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1612765288909!5m2!1sen!2sin" width="1000" height="1500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                 
                                  </div> 
                                </body>
                              </html>
                        `,
                      }}
                    style={{ backgroundColor: '#fff', height }}
                /> */}
                <ImageBackground resizeMode={'stretch'} // or cover
                    style={styles.barDetailImg} // must be passed from the parent, the number may vary depending upon your screen size
                    source={Images.map1}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: '5%' }}>
                        <TouchableOpacity style={{ position: 'absolute', left: width * 0.7, top: height * 0.23 }} onPress={() => this.props.navigation.navigate('CropListScreen')}>

                            <Image source={Images.locationRed} style={styles.warnIcon} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ position: 'absolute', left: width * 0.43, top: height * 0.30 }} onPress={() => this.props.navigation.navigate('CropListScreen')}>

                            <Image source={Images.locationYellow} style={styles.warnIcon} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ position: 'absolute', left: width * 0.48, top: height * 0.4 }} onPress={() => this.props.navigation.navigate('Home')}>

<Image source={Images.locationGrey} style={styles.warnIcon} />
</TouchableOpacity>


                    </View>
                </ImageBackground>


            </SafeAreaView>
        );
    }
}
export default DashboardMap;
