import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList
} from "react-native";
const { width, height } = Dimensions.get('window');
import styles from './style';
const dropShadow = {
    width: width * 0.9,
    height: 80,
    color: "#000",
    border: 1,
    radius: 40,
    overflow: 'hidden',
    opacity: 1,
    x: 10,
    y: 49,
    style: {
        marginRight: width * 0.05,
        //   marginTop:height*0.1, 
        //   borderBottomRightRadius: width * 0.1,
        //    borderBottomLeftRadius: width * 0.1,
        alignSelf: 'center',
    }
}
class SelectLanguage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected_language: 'English',
            language: [{ "date": "English" }, { "date": "Hindi" }, { "date": "Punjabi" }, { "date": "Marathi" }, { "date": "Gujrati" }, { "date": "Malyalam" },],
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.wrapper}>
                    <View style={styles.upperWrapper}>
                        <Text style={{ fontSize: 24, marginTop: 20, color: '#fff' }}>SELECT LANGUAGE</Text>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: '10%' }}>
                    <FlatList
                        numColumns={2}
                        extraData={this.state}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.language}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => this.setState({ selected_language: item.date })}>
                                <View style={item.date == this.state.selected_language ?
                                    {
                                        padding: 10, justifyContent: 'center', backgroundColor: '#006631', borderRadius: 20, marginRight: 10, minWidth: width * 0.35, minHeight: height * 0.15, margin: 5
                                    }
                                    :
                                    {
                                        padding: 10, justifyContent: 'center', backgroundColor: '#ffffff', borderRadius: 20, marginRight: 10, minWidth: width * 0.35, minHeight: height * 0.15, margin: 5
                                    }
                                }>
                                    <Text style={item.date == this.state.selected_language ? { fontFamily: 'Eina03_Bold', alignSelf: 'center', fontSize: 26, color: 'white' } : { fontFamily: 'Eina03_Bold', alignSelf: 'center', fontSize: 26, color: 'black' }}>{item.date}</Text>
                                </View></TouchableOpacity>} />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 8 }}>
                    <Text style={{ fontSize: 20 }}>English Selected</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10, height: '20%' }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={styles.boxBtn2}
                        onPress={() => this.props.navigation.navigate('PincodeScreen')}>
                        <Text style={styles.btnText}> CLICK TO CONFIRM </Text>
                    </TouchableOpacity>
                </View>
                {/* <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <View style={styles.downWrapper}>
                    </View>
                </View>
                */}
            </View>
        );
    }
}
export default SelectLanguage;
