import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet, TextInput
} from "react-native";
import Navigation from "./src/Navigation";
import {store} from './src/redux/store';
import AsyncStorage from '@react-native-community/async-storage';
import {Provider} from 'react-redux';

class App extends Component {
  render() {
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
    TextInput.defaultProps = TextInput.defaultProps || {};
    TextInput.defaultProps.allowFontScaling = false;
    return (
      <>
 <Provider store={store}>
     
     <Navigation/>
     </Provider>
      </>
    );
  }
}
export default App;

