import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const login = (login) => {
  return async (dispatch, getState) => {
    
    url = urlObj.login_url+"?email="+login.email+"&password="+login.password+"&device_token="+login.device_token+"&mobile="+login.mobile+"&imei="+login.imei+"&mobile_os="+login.mobile_os;
    console.log(url)
    await axios
      .post(url)
      .then(async (response) => {
        // console.log(response.data.data)
        await AsyncStorage.setItem('token', response.data.data.token);
        await AsyncStorage.setItem('name', response.data.data.user.name);
        await AsyncStorage.setItem('phone', response.data.data.user.mobile);
        dispatch({
          type: 'LOGIN',
          payload: response.data.data
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};