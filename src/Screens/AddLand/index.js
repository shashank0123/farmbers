import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Modal,
  I18nManager,
  RefreshControl,
  fetchData,
  Alert,ImageBackground
} from "react-native";
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils';
import SplashPub from "../SplashPub";
import styles from './style';

class AddLands extends Component {
  constructor(props) {
    super(props);
    this.state = {
        show: false,
        showWarn:false,
        showAlert: false,
        loadingSpl: true,
    };
}

componentDidMount() {
  this.showSpl();
}

// componentWillUnmount() {
//   clearTimeout(this.splTimeout);
 
// }

showSpl = () => {
  this.splTimeout = setTimeout(() => {
    this.setState({ loadingSpl:false, });
    
  }, 4000);
}
    
  render() {
    if(this.state.loadingSpl) {
      return <SplashPub/>
    }
    return (
      
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.backContainerStyle} />
            </TouchableOpacity>
          </View>
          <View style={styles.logoContainer}>
            <Image source={Images.farmLogo} style={styles.logoStyle} />
          </View>
          <View style={{ position: 'absolute', left: '70%', flexDirection: 'row' }}>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('NotificationScreen')}>
              <Image source={Images.bell} style={styles.bellIcon1} />
            </TouchableOpacity>
            <View>
              <TouchableOpacity onPress={() =>this.props.navigation.navigate('Profile')}>
                <Image source={Images.profilePic} style={styles.profileIcon} />
              </TouchableOpacity>
            </View>
            <View style={{ marginLeft: 8 }}>
              <TouchableOpacity onPress={() =>
                this.props.navigation.navigate('ReferAndEarn')}>
                <Image source={Images.refer} style={styles.bellIcon} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.wrapper}>
          <View style={styles.upperWrapper}>
            <Text style={{ fontSize: 24, marginTop: 10, color: '#fff' }}>Add Lands</Text>
          </View>
        </View>
        <View style={{ marginTop: height * 0.03, alignItems: 'center',marginLeft:10,marginRight:10 }}>
        <View style={styles.formBox}>
                            <Image source={Images.dummy} style={styles.searchIcon} />
                            <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                style={styles.input} placeholder="Enter Farm Name"
                            />
                        </View>
                        {/* <View style={styles.formBox}>
                            <Image source={Images.location} style={styles.searchIcon} />
                            <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                style={styles.input} placeholder="Location"
                            />
                        </View> */}
            </View>



            <View style={{width:'100%',justifyContent:'center',alignItems:'center',marginTop:'5%'}} >
            <ImageBackground resizeMode={'stretch'} // or cover
                    style={styles.barDetailImg} // must be passed from the parent, the number may vary depending upon your screen size
                    source={Images.map1}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: '5%' }}>
                        <TouchableOpacity style={{ position: 'absolute', left: width * 0.725, top: height * 0.295 }} onPress={() => this.props.navigation.navigate('CropListScreen')}>

                            <Image source={Images.locationGreen} style={styles.warnIcon} />
                        </TouchableOpacity>
                       
                      

                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-around',marginTop:height*0.47}}>                     
                         <View style={{ justifyContent: 'center', alignItems: 'center',marginTop:'10%'}}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.boxBtn2}
                            onPress={() => this.props.navigation.navigate('AddLand')}
                            >
                                <Text style={styles.btnText}>Farmbers Circle </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center',marginTop:'10%'}}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.boxBtn2}
                            onPress={() => this.props.navigation.navigate('Home')}
                            >
                                <Text style={styles.btnText}>Submit </Text>
                            </TouchableOpacity>
                        </View>
                        </View>
                </ImageBackground>
                      </View>




                  

            
       
     
      </View>
    );
  }
}
export default AddLands;
