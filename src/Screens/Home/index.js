import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, FlatList
} from "react-native";
const { width, height } = Dimensions.get('window');
import * as Animatable from 'react-native-animatable';
import { Images } from '../../utils'
import styles from './style';
import SpalshBstCrp from "../SpalshBstCrp";
import BouncingBalls from 'react-native-bouncing-ball'
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingSpl: true,
        };
    }
    componentDidMount() {
        this.showSpl();
    }
    // componentWillUnmount() {
    //   clearTimeout(this.splTimeout);
    // }
    showSpl = () => {
        this.splTimeout = setTimeout(() => {
            this.setState({ loadingSpl: false, });
        }, 7000);
    }
    render() {
        if (this.state.loadingSpl) {
            return <SpalshBstCrp />
        }
        return (
            <View style={styles.container}>
                <View style={styles.upperBar}>
                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.backContainerStyle} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.logoContainer}>
                        <Image source={Images.farmLogo} style={styles.logoStyle} />
                    </View>
                    <View style={{ position: 'absolute', left: '70%', flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('NotificationScreen')}>
                            <Image source={Images.bell} style={styles.bellIcon1} />
                        </TouchableOpacity>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={Images.profilePic} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginLeft: 8 }}>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('ReferAndEarn')}>
                                <Image source={Images.refer} style={styles.bellIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                            <View style={styles.upperWrapper}>
                                <Text style={{ fontSize: 24, color: '#fff' }}>Best Crops for you</Text>
                            </View>
                            <View style={{ width: '95%', alignItems: 'flex-end', marginTop: '8%' }}>
                                {/* <Animatable.View animation="bounceInLeft" duration={4000}
                                    style={{ flex: 1, }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AnalysisScreen')} style={{}}>
                                        <View style={{ padding: 10, justifyContent: 'center', backgroundColor: '#006631', borderRadius: 100, marginRight: 10, minWidth: height * 0.25, minHeight: height * 0.2 }}>
                                            <Image source={Images.bhindi} style={{ height: height * 0.25, width: height * 0.25, borderRadius: 100, }} />
                                        </View></TouchableOpacity>
                                </Animatable.View> */}
                                <Animatable.View animation="bounceInRight" duration={4000}
                                    style={{ flex: 1, }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AnalysisScreen')} style={{}}>
                                        <View style={{ padding: 10, justifyContent: 'center', backgroundColor: '#006631', borderRadius: 100, marginRight: 10, minWidth: height * 0.2, minHeight: height * 0.2 }}>
                                            <Image source={Images.bhindi} style={{ height: height * 0.22, width: height * 0.22, borderRadius: 100, }} />
                                        </View></TouchableOpacity>
                                </Animatable.View>
                            </View>
                            <View style={{ width: '95%', alignItems: 'flex-start', }}>
                                <Animatable.View animation="bounceInRight" duration={4000}
                                    style={{ flex: 1, }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AnalysisScreen')} style={{}}>
                                        <View style={{ padding: 10, justifyContent: 'center', backgroundColor: '#006631', borderRadius: 100, marginRight: 10, minWidth: height * 0.2, minHeight: height * 0.2 }}>
                                            <Image source={Images.tomata} style={{ height: height * 0.2, width: height * 0.2, borderRadius: 100, }} />
                                        </View></TouchableOpacity>
                                </Animatable.View>
                            </View>
                            <View style={{ width: '95%', alignItems: 'flex-end', marginBottom: 20 }}>
                                <Animatable.View animation="bounceInLeft" duration={4000}
                                    style={{ flex: 1, }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AnalysisScreen')} style={{}}>
                                        <View style={{ padding: 10, justifyContent: 'center', backgroundColor: '#006631', borderRadius: 100, marginRight: 10, minWidth: height * 0.15, minHeight: height * 0.15 }}>
                                            <Image source={Images.caps} style={{ height: height * 0.15, width: height * 0.15, borderRadius: 100, }} />
                                        </View></TouchableOpacity>
                                </Animatable.View>
                            </View>
                        </View>
                        {/* <View style={styles.downWrapper}>
                        </View> */}
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
export default Home;
``