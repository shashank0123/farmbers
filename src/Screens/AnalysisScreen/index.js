import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList
} from "react-native";
import {
    LineChart,
} from "react-native-chart-kit";
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils'
import styles from './style';
import { Container, Header, Tab, Tabs, TabHeading, Icon } from 'native-base';
class AnalysisScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <View style={styles.container}>
              
                <View style={styles.upperBar}>
                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.backContainerStyle} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.logoContainer}>
                        <Image source={Images.farmLogo} style={styles.logoStyle} />
                    </View>
                    <View style={{ position: 'absolute', left: '70%', flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() =>
                            this.props.navigation.navigate('NotificationScreen')}>
                            <Image source={Images.bell} style={styles.bellIcon1} />
                        </TouchableOpacity>
                        <View>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                                <Image source={Images.profilePic} style={styles.profileIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginLeft: 8 }}>
                            <TouchableOpacity onPress={() =>
                                this.props.navigation.navigate('ReferAndEarn')}>
                                <Image source={Images.refer} style={styles.bellIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                            <View style={styles.upperWrapper}>
                                <Text style={{ fontSize: 24, color: '#fff' }}>Bhindi</Text>
                            </View>
                            <Tabs tabBarUnderlineStyle={{ backgroundColor: 'yellow' }} tabContainerStyle={{ backgroundColor: '#006631', marginTop: 10 }}>
                                <Tab heading={<TabHeading style={{ backgroundColor: '#006631' }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 20, marginTop: 5, color: '#fff', }}>History</Text>
                                    </View>
                                </TabHeading>} activeTextStyle={{ color: '#000', fontWeight: 'normal' }} activeTabStyle={{ backgroundColor: '#000' }}>
                                    <View style={{ height: height * 1 }}>
                                        <View style={{ width: '100%', marginTop: '7%' }}>
                                            <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Average Demand</Text>
                                            <LineChart
                                                data={{
                                                    labels: ["Jan", "Feb", "March", "April", "May", "June"],
                                                    datasets: [
                                                        {
                                                            data: [
                                                                Math.random() * 100,
                                                                Math.random() * 100,
                                                                Math.random() * 100,
                                                                Math.random() * 100,
                                                                Math.random() * 100,
                                                                Math.random() * 100
                                                            ]
                                                        }
                                                    ]
                                                }}
                                                width={Dimensions.get("window").width} // from react-native
                                                height={170}
                                                yAxisLabel="+"
                                                yAxisSuffix="'c"
                                                yAxisInterval={1} // optional, defaults to 1
                                                chartConfig={{
                                                    backgroundColor: "#F5F5F5",
                                                    backgroundGradientFrom: "#F5F5F5",
                                                    backgroundGradientTo: "#F5F5F5",
                                                    decimalPlaces: 0, // optional, defaults to 2dp
                                                    color: (opacity = 1) => '#DCDCDC',
                                                    labelColor: (opacity = 1) => '#808080',
                                                    style: {
                                                        // borderRadius: 10,
                                                    },
                                                    propsForDots: {
                                                        r: "3",
                                                        strokeWidth: "1",
                                                        stroke: "#ffa726"
                                                    }
                                                }}
                                                bezier
                                                style={{
                                                    marginVertical: 20,
                                                    // borderRadius: 16
                                                }}
                                            />
                                        </View>
                                        <View>
                                            <View style={{ width: '100%', marginTop: '1%' }}>
                                                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Average Supply</Text>
                                                <LineChart
                                                    data={{
                                                        labels: ["Jan", "Feb", "March", "April", "May", "June"],
                                                        datasets: [
                                                            {
                                                                data: [
                                                                    Math.random() * 100,
                                                                    Math.random() * 100,
                                                                    Math.random() * 100,
                                                                    Math.random() * 100,
                                                                    Math.random() * 100,
                                                                    Math.random() * 100
                                                                ]
                                                            }
                                                        ]
                                                    }}
                                                    width={Dimensions.get("window").width} // from react-native
                                                    height={180}
                                                    yAxisLabel="+"
                                                    yAxisSuffix="'c"
                                                    yAxisInterval={1} // optional, defaults to 1
                                                    chartConfig={{
                                                        backgroundColor: "#F5F5F5",
                                                        backgroundGradientFrom: "#F5F5F5",
                                                        backgroundGradientTo: "#F5F5F5",
                                                        decimalPlaces: 0, // optional, defaults to 2dp
                                                        color: (opacity = 1) => '#DCDCDC',
                                                        labelColor: (opacity = 1) => '#808080',
                                                        style: {
                                                            // borderRadius: 10,
                                                        },
                                                        propsForDots: {
                                                            r: "3",
                                                            strokeWidth: "1",
                                                            stroke: "#ffa726"
                                                        }
                                                    }}
                                                    bezier
                                                    style={{
                                                        marginVertical: 20,
                                                        // borderRadius: 16
                                                    }}
                                                />
                                            </View>
                                        </View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: height * 0.02 }}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={styles.boxBtn2}
                                    onPress={() => this.props.navigation.navigate('CropLockScreen')}>
                                    <Text style={styles.btnText}>Click to Lock Bhindi </Text>
                                </TouchableOpacity>
                            </View>

                                    </View>
                                </Tab>
                                <Tab heading={<TabHeading style={{ backgroundColor: '#006631' }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                                        <Text style={{ fontSize: 20, marginTop: 5, color: '#fff' }}>Forecast</Text>
                                    </View>
                                </TabHeading>}>
                                    <View style={styles.TabView}>
                                        <View style={{ height: height * 1 }}>
                                            <View style={{ width: '100%', marginTop: '7%' }}>
                                                <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Average Demand</Text>
                                                <LineChart
                                                    data={{
                                                        labels: ["Jan", "Feb", "March", "April", "May", "June"],
                                                        datasets: [
                                                            {
                                                                data: [
                                                                    Math.random() * 100,
                                                                    Math.random() * 100,
                                                                    Math.random() * 100,
                                                                    Math.random() * 100,
                                                                    Math.random() * 100,
                                                                    Math.random() * 100
                                                                ]
                                                            }
                                                        ]
                                                    }}
                                                    width={Dimensions.get("window").width} // from react-native
                                                    height={200}
                                                    yAxisLabel="+"
                                                    yAxisSuffix="'c"
                                                    yAxisInterval={1} // optional, defaults to 1
                                                    chartConfig={{
                                                        backgroundColor: "#F5F5F5",
                                                        backgroundGradientFrom: "#F5F5F5",
                                                        backgroundGradientTo: "#F5F5F5",
                                                        decimalPlaces: 0, // optional, defaults to 2dp
                                                        color: (opacity = 1) => '#DCDCDC',
                                                        labelColor: (opacity = 1) => '#808080',
                                                        style: {
                                                            // borderRadius: 10,
                                                        },
                                                        propsForDots: {
                                                            r: "3",
                                                            strokeWidth: "1",
                                                            stroke: "#ffa726"
                                                        }
                                                    }}
                                                    bezier
                                                    style={{
                                                        marginVertical: 20,
                                                        // borderRadius: 16
                                                    }}
                                                />
                                            </View>
                                            <View>
                                                <View style={{ width: '100%', marginTop: '7%' }}>
                                                    <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Average Supply</Text>
                                                    <LineChart
                                                        data={{
                                                            labels: ["Jan", "Feb", "March", "April", "May", "June"],
                                                            datasets: [
                                                                {
                                                                    data: [
                                                                        Math.random() * 100,
                                                                        Math.random() * 100,
                                                                        Math.random() * 100,
                                                                        Math.random() * 100,
                                                                        Math.random() * 100,
                                                                        Math.random() * 100
                                                                    ]
                                                                }
                                                            ]
                                                        }}
                                                        width={Dimensions.get("window").width} // from react-native
                                                        height={200}
                                                        yAxisLabel="+"
                                                        yAxisSuffix="'c"
                                                        yAxisInterval={1} // optional, defaults to 1
                                                        chartConfig={{
                                                            backgroundColor: "#F5F5F5",
                                                            backgroundGradientFrom: "#F5F5F5",
                                                            backgroundGradientTo: "#F5F5F5",
                                                            decimalPlaces: 0, // optional, defaults to 2dp
                                                            color: (opacity = 1) => '#DCDCDC',
                                                            labelColor: (opacity = 1) => '#808080',
                                                            style: {
                                                                // borderRadius: 10,
                                                            },
                                                            propsForDots: {
                                                                r: "3",
                                                                strokeWidth: "1",
                                                                stroke: "#ffa726"
                                                            }
                                                        }}
                                                        bezier
                                                        style={{
                                                            marginVertical: 20,
                                                            // borderRadius: 16
                                                        }}
                                                    />
                                                </View>
                                            </View>
                                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: height * 0.02 }}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={styles.boxBtn2}
                                    onPress={() => this.props.navigation.navigate('CropLockScreen')}>
                                    <Text style={styles.btnText}>Click to Lock Bhindi </Text>
                                </TouchableOpacity>
                            </View>

                                        </View>
                                       
                                    </View>
                                </Tab>
                            </Tabs>
                        </View>
                        {/* <View style={styles.downWrapper}>
                        </View> */}
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
export default AnalysisScreen;
``