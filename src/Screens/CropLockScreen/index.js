import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList
} from "react-native";
const { width, height } = Dimensions.get('window');
import CheckBox from '@react-native-community/checkbox';
import DateTimePicker from '@react-native-community/datetimepicker';
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
class CropLockScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>Farmbers</Text>
                    </View>
                    <View style={{ justifyContent: 'center', width }}>
                        <TouchableOpacity
                            style={styles.profileContainer}
                            onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                            <Image source={Images.profilePic} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View> */}
                 <View style={styles.upperBar}>
                <View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
               
                <Image source={Images.backIcon} style={styles.backContainerStyle} />
                </TouchableOpacity>

                </View>
              <View style={styles.logoContainer}>
                 
                  <Image source={Images.farmLogo} style={styles.logoStyle} />
                 
               
              </View>
            
              <View style={{ position:'absolute', left:'70%',flexDirection:'row'}}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('NotificationScreen')}>
                  <Image source={Images.bell} style={styles.bellIcon1} />
                </TouchableOpacity>
            
              <View>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('Profile')}>
                  <Image source={Images.profilePic}  style={styles.profileIcon}/>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 8 }}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('ReferAndEarn')}>
                  <Image source={Images.refer} style={styles.bellIcon} />
                </TouchableOpacity>
              </View>
              </View>
            </View>
                <ScrollView
                    // contentContainerStyle={styles.scrollWrapper}
                    showsVerticalScrollIndicator={false}>
                    <View style={styles.scrollWrapper}>
                        <View style={styles.docDetailedWrapper2}>
                            <View style={styles.middleWrapper}>
                                
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                                <Text style={{ textAlign:'center', fontSize: 32,fontWeight:'normal'}}>Are you sure you want to lock bhindi </Text>
                            </View>
                        </View>
                        <View style={styles.bottomBar}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CropStage')}>
                                <Image source={Images.yes} style={styles.searchIcon} />
                            </TouchableOpacity>
                           
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('AnalysisScreen')}>
                                <Image source={Images.no} style={styles.searchIcon} />
                            </TouchableOpacity>
                          
                        </View>
                        {/* <View style={styles.downWrapper}>
                        </View> */}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default CropLockScreen;
