/*This is an Example of Animated Splash Screen*/
import React, { Component } from 'react';
import { View, Text, Image, Animated,Dimensions,  } from 'react-native';
import { Images } from '../../utils'
const { width, height } = Dimensions.get('window');

export default class SplashDDCLM extends Component {
  constructor() {
    super();
    this.state = {
      animating: false,
      alignsecond: false,
      loadingTIL: true,
      loadingNL: false,

    };
    setTimeout(
      () =>{
        this.setState({ align: 'flex-start',loadingTIL: false, loadingNL: true , }, function() {
          this.setState({
            alignsecond: true,
            
          });
        }),this.animatedBox()},
      2000
    );
    setTimeout(
        () =>{
            // this.props.navigation.navigate("CropStage")
        },1000
    )
  }

  componentDidMount(){
    this.animatedWidth = new Animated.Value(300)
    this.animatedHeight = new Animated.Value(300)
  }

  animatedBox = () => {
    Animated.timing(this.animatedWidth, {
       toValue: 200,
       duration: 1000,
       useNativeDriver:false
    }).start()
    Animated.timing(this.animatedHeight, {
       toValue: 200,
       duration: 1000,
       useNativeDriver:false
    }).start()
 }



  render() {
    const animatedStyle = { width: this.animatedWidth, height: this.animatedHeight, alignItems:'center', justifyContent:'center' }
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          flexDirection: 'column',
          marginHorizontal: 40,
          justifyContent: 'center'
        }}>
          
        <Animated.View style = {[animatedStyle]}>
       
        <Image source={Images.farmLogo} style={{ width: width * 0.65, height: height * 0.1, }} />
        </Animated.View>
        {!this.state.alignsecond ? null : (
          <View style={{ margin: 10 ,alignItems:'center', marginTop:50,width}}>
            <Text
              style={{ color: '#006631', fontSize: 20, fontWeight: 'bold' }}>
              Publishing DDCLM...
            </Text>
         
            <Text
              style={{ color: '#006631', fontSize:16, fontWeight: 'bold',marginTop:15,}}>
          (Dynamic Digital Cropping Lifecycle Management)
            </Text>
            {/* <Text
              style={{ color: '#006631', fontSize: 20, fontWeight: 'bold',marginTop:5 }}>
             Location...
            </Text> */}
          </View>
        )}
      </View>
    );
  }
}