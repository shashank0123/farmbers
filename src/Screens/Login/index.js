import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker
} from "react-native";
const { width, height } = Dimensions.get('window');
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import LinearGradient from 'react-native-linear-gradient';
import styles from './style';
import AsyncStorage from '@react-native-community/async-storage';
import { login } from '../../redux/actions/login';
import { connect } from 'react-redux';
class Login extends Component {
    // state = {  email: 'satyam', password: '1234', device_token: "", imei: "",  }
    constructor(props) {
        super(props);
        this.state = {
            hidePassword:true,
            email:'', password:false, device_token: "", imei : "",
            mobile_os : ''
        }
        this._getStorageValue();
    }
    async _getStorageValue(){
        var value = await AsyncStorage.getItem('token')
        var fcm = await AsyncStorage.getItem('fcmToken')
        var mobile_os = await AsyncStorage.getItem('mobile_os')
        this.setState({device_token: fcm})
        this.setState({mobile_os: mobile_os})
        if (value != null){
                    console.log(value)
                  this.props.navigation.replace('DashboardMap')
               }
    
        return value
      }
      submitLogin() {
        console.log(this.state)
          this.props.login(this.state).then(async ()=>{
            var value = await AsyncStorage.getItem('token')
        if (value != null){
                    console.log(value)
                   
                  this.props.navigation.replace('DashboardMap')
               }
          })
    
    
        }
    
    render() {
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView behavior="padding">
                    <View style={styles.header}>
                       
                        <Image source={Images.farmLogo} style={{ width: width * 0.65, height: height * 0.1, }} />
                    </View>
                   
                    <View style={{ marginTop: height * 0.2, alignItems: 'center', }}>
                        <View style={styles.formBox}>
                            <Image source={Images.phone} style={styles.searchIcon} />
                            <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                style={styles.input} placeholder="Mobile No./Farmbers Id"
                                onChangeText={email => this.setState({email})}
                                value={this.state.email}
                            />
                        </View>
                        <View style={styles.formBox}>
                            <Image source={Images.password} style={styles.searchIcon} />
                            <TextInput placeholderTextColor="#000"
                                style={styles.input} placeholder="PIN" secureTextEntry
                                onChangeText={password => this.setState({ password })}
                               
                                value={this.state.password} 
                                />
                        </View>
                    </View>
                    <TouchableOpacity style={{ alignItems: 'flex-end', marginRight: 20, marginTop: 10 }}
                        onPress={() =>
                            this.props.navigation.navigate('ForgetPassword')
                        }>
                        <Text style={styles.forgetText}>
                            Forget PIN?
                                </Text>
                    </TouchableOpacity>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: height * 0.06 }}>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={styles.boxBtn2}
                            onPress={() => this.submitLogin()}
                            // onPress={() =>
                            //     this.props.navigation.navigate('DashboardMap')
                            // }

                            >
                            <Text style={styles.btnText}> SUBMIT</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontSize: 12, marginTop: 15, textAlign: 'center' }}>SignUp with</Text>
                    <View style={{ flexDirection: 'row', paddingTop: 10, alignContent: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity 
                        // onPress={() => this.props.navigation.navigate('Profile')}
                            >
                            <Image source={Images.googleLogo} style={styles.profileIcon} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ marginLeft: 10 }}
                            // onPress={() => this.props.navigation.navigate('Profile')}
                            >
                            <Image source={Images.fbLogo} style={styles.fbIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: height * 0.05 }}>
                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                            onPress={() =>
                                this.props.navigation.navigate('Ragistration')
                            }>
                            <Text>
                                <Text> Don't Have an Account?</Text>
                                <Text style={{ fontWeight: 'normal', color: 'red' }}> Sign Up</Text>
                            </Text>
                        </TouchableOpacity>
                        <View style={{flexDirection:'row',justifyContent:'center',marginTop:'8%'}}>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() =>
                                        this.props.navigation.navigate('TermsAndConditions')
                                    }>
                                    <Text style={styles.forgetText1}>
                                        <Text > Terms & Conditions </Text>
                                    </Text>
                                </TouchableOpacity>
                                <Text style={{fontWeight:'bold'}}>||</Text>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() =>
                                        this.props.navigation.navigate('PrivacyPolicyScreen')
                                    }>
                                    <Text style={styles.forgetText1}>
                                        <Text > Privacy Policy </Text>
                                    </Text>
                                </TouchableOpacity>
                                </View>
                    </View>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      login1: state.login.login
    };
  };
  export default connect(mapStateToProps, {
    login
  })(Login);
  
