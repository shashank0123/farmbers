import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList,
    Modal,
} from "react-native";
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils'
import styles from './style';
import AwesomeAlert from 'react-native-awesome-alerts';
import SplashDDCLM from "../SplashDDCLM"
const Data = [
  {
      id: '1',
      name: 'Field Prepration',
     date:'12 aug 2020',
     month:'Aug',
     dateRange:'18-20'
  },
  {
      id: '2',
      name: 'Irrigagtion',
      date:'12 aug 2020',
      month:'Aug',
      dateRange:'20-25'
  },
  {
    id: '3',
    name: 'Weeding',
    date:'12 aug 2020',
    month:'Aug',
    dateRange:'25-30'
},
{
  id: '4',
  name: 'Seeding',
  date:'12 aug 2020',
  month:'Sep',
  dateRange:'15-20'
},
{
  id: '5',
  name: 'Harvest',
  date:'12 aug 2020',
  month:'Sep',
  dateRange:'25-27'
},
];
class CropStage extends Component {
   
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            showWarn:false,
            showAlert: false,
            loadingSpl: true,
        };
    }
    componentDidMount() {
      this.showSpl();
    }
  
    // componentWillUnmount() {
    //   clearTimeout(this.splTimeout);
     
    // }
    showSpl = () => {
      this.splTimeout = setTimeout(() => {
        this.setState({ loadingSpl:false, });
        
      }, 5000);
    }
    showAlert = () => {
      this.setState({
        showAlert: true
      });
    };
   
    hideAlert = () => {
      this.setState({
        showAlert: false
      });
      this.props.navigation.replace('AddLand')
    };
    hideAlert1 = () => {
      this.setState({
        showAlert: false
      });
      this.props.navigation.replace('DashboardMap')
    };
    render() {
      const {showAlert} = this.state;
      if(this.state.loadingSpl) {
        return <SplashDDCLM/>
      }
        return (
            <View style={styles.container}>
               
                 <View style={styles.upperBar}>
                <View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
               
                <Image source={Images.backIcon} style={styles.backContainerStyle} />
                </TouchableOpacity>
                </View>
              <View style={styles.logoContainer}>
                 
                  <Image source={Images.farmLogo} style={styles.logoStyle} />
                 
               
              </View>
            
              <View style={{ position:'absolute', left:'70%',flexDirection:'row'}}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('NotificationScreen')}>
                  <Image source={Images.bell} style={styles.bellIcon1} />
                </TouchableOpacity>
            
              <View>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('Profile')}>
                  <Image source={Images.profilePic}  style={styles.profileIcon}/>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 8 }}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('ReferAndEarn')}>
                  <Image source={Images.refer} style={styles.bellIcon} />
                </TouchableOpacity>
              </View>
              </View>
            </View>
                <ScrollView
                    // contentContainerStyle={styles.scrollWrapper}
                    showsVerticalScrollIndicator={false}>
                    <View style={styles.scrollWrapper}>
                        <View style={{ marginTop: height * 0.05,margin:10 }}>
                        <FlatList
                        // style=
                        // {{ marginBottom: '14%' }}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ marginTop: '2%',paddingLeft:5 }}
                        data={Data}
                        renderItem={({ item }) => (
                            <>
                               <View style={[styles.formBox,{backgroundColor:'#D3D3D3'}]}> 
                                    
                                    <View style={{ padding: 10, justifyContent: 'center', backgroundColor: '#006631', borderRadius: 100,  minWidth: height * 0.075, minHeight: height * 0.075,alignItems:'center' }}>
                                    <Text style={{color:'#fff',fontWeight:'bold'}}>{item.month}</Text>
                                      <Text style={{color:'#fff',fontSize:10,fontWeight:'bold'}}>{item.dateRange}</Text>
                                     
                                         
                                      </View>
                                    <TouchableOpacity style={styles.input} onPress={() => { this.setState({ show: true }) }}>
                               
                                      <Text   numberOfLines={1} style={{ fontSize: 18,}} >{item.name}</Text>
                                      {/* <Text style={{ fontSize: 10 }} >{item.date}</Text> */}
                                
                                </TouchableOpacity> 
                                 
                              </View>
                              
                            </>
                        )}
                        keyExtractor={item => item.id}
                    />
             
                        </View>
                           {/*Modal Begin For Discription  */}
                 <Modal
              transparent={true}
              animationType='slide'
              visible={this.state.show}>
              <View style={{ backgroundColor: '#000000aa', height: height *1 }}>
                <TouchableOpacity
                  style={styles.testModel}
                  onPress={() => { this.setState({ show: false }) }}
                >
                  <View style={styles.middleWrapper1}>
                    <View style={{ width: '100%', height: '20%', justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={styles.nameWrapper} >Irrigation</Text>
                    </View>
                  </View>
                  <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                                <Text style={{ textAlign: 'left', fontSize: 18 }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release of
                               
                               .</Text>
                            </View>
                </TouchableOpacity>
              </View>
            </Modal>
            {/* Modal END */}
             
                        <View style={{ justifyContent: 'center', alignItems: 'center',}}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.boxBtn2}
                            // onPress={() => this.props.navigation.navigate('DashboardMap')}
                            
                            onPress={() => {
                              this.showAlert();
                            }}>
                                <Text style={styles.btnText}> DONE </Text>
                            </TouchableOpacity>
                            <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Alert"
          message="Want to grow more Crops?"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="   No   "
          confirmText="   Yes   "
          confirmButtonColor="#006631"
          onCancelPressed={() => {
            this.hideAlert1();
          }}
          onConfirmPressed={() => {
            this.hideAlert();
          }}
        />
                        </View>
                        {/* <View style={styles.downWrapper}>
                        </View> */}
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default CropStage;
