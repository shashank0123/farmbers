import React, { Component } from "react";
import { 
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,
    
    FlatList,
    Modal,
    I18nManager,
    RefreshControl,
    fetchData,
} from "react-native";
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils';
import styles from './style';


const Data= [
  {
      id: '1',
      name: 'Lok Shabha',
      image: Images.parlmnt
  },
  {
      id: '2',
      name: 'Lok Shabha',
      image: Images.parlmnt
  },
 

];




class Mandi extends Component {
    render() {
        return (
            <View style={styles.container}>
            <View style={styles.upperBar}>
                <View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
               
                <Image source={Images.backIcon} style={styles.backContainerStyle} />
                </TouchableOpacity>

                </View>
              <View style={styles.logoContainer}>
                 
                  <Image source={Images.farmLogo} style={styles.logoStyle} />
                 
               
              </View>
            
              <View style={{ position:'absolute', left:'70%',flexDirection:'row'}}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('NotificationScreen')}>
                  <Image source={Images.bell} style={styles.bellIcon1} />
                </TouchableOpacity>
            
              <View>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('Profile')}>
                  <Image source={Images.profilePic}  style={styles.profileIcon}/>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 8 }}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('ReferAndEarn')}>
                  <Image source={Images.refer} style={styles.bellIcon} />
                </TouchableOpacity>
              </View>
              </View>
            </View>
            <View style={styles.wrapper}>
                    <View style={styles.upperWrapper}>
                        <Text style={{ fontSize: 24, marginTop: 10, color: '#fff' }}>Buyers</Text>
                    </View>
                </View>
               
                <View style={{flexDirection:'row',marginTop:'15%',}}>
                  <View style={{width:'50%',}}>
                    <Text style={{fontSize:30, fontWeight:'bold', marginLeft:'30%',color:'#006631'}}>Mandi</Text>
                    <Text style={{fontSize:10, fontWeight:'bold',marginLeft:'10%',marginRight:10,marginTop:10}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae felis augue. Nullam tempor nisi a nisl placerat, et congue sapien dictum. Ut laoreet viverra felis. Nullam leo nisl, feugiat nec justo sit amet, ultricies lacinia neque. Aliquam iaculis efficitur ultricies. </Text>
                  </View>
                  <View style={{width:'50%',}}>
                <Image source={Images.mandiPic} style={{ width: width * 0.45, height: height * 0.25,borderWidth:2,borderColor:'#006631',borderRadius:10 }} />
                </View>



</View>
<View style={{flexDirection:'row',marginTop:40,}}>
                  <View style={{justifyContent:'center',alignItems:'center'}}>
                  <Image source={Images.homeKit} style={{ width: width * 0.3, height: height * 0.2,marginLeft:10,marginRight:10,borderWidth:2,borderColor:'#006631',borderRadius:10 }} />
                  <Text style={{color:'#006631',fontWeight:'bold',}}>Home Kitchen</Text>
                  </View>
                  <View style={{justifyContent:'center',alignItems:'center'}}>
                <Image source={Images.hotelKitchen} style={{ width: width * 0.3, height: height * 0.2,marginRight:10,borderWidth:2,borderColor:'#006631',borderRadius:10 }} />
                <Text style={{color:'#006631',fontWeight:'bold',}}>Restaurant Kitchen</Text>
                </View>
                <View style={{justifyContent:'center',alignItems:'center'}}>
                <Image source={Images.shopVeg} style={{ width: width * 0.3, height: height * 0.2,borderWidth:2,borderColor:'#006631',borderRadius:10 }} />
                <Text style={{color:'#006631',fontWeight:'bold',}}> Veg Shop</Text>
                </View>



</View>
<View style={{marginTop:'10%',borderWidth:1,borderColor:'#006631',padding:10,width:'90%',marginLeft:20,backgroundColor:'#D3D3D3',justifyContent:'center',alignItems:'center',borderRadius:5,flexDirection:'row'}}>
<Image source={Images.star} style={{ width: width * 0.05, height: height * 0.023, }} />
  <Text style={{color:'#006631',fontWeight:'bold',marginLeft:3,marginRight:3}}>Ask for Premium version to Unlock this features</Text>
  <Image source={Images.star} style={{ width: width * 0.05, height: height * 0.023,}} />
</View>

            </View>
        );
    }
}
export default Mandi;


