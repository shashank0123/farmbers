import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList
} from "react-native";
const { width, height } = Dimensions.get('window');
import DateTimePicker from '@react-native-community/datetimepicker';
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <View style={styles.upperBar}>
                    <View style={styles.backIconContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={Images.backIcon} style={styles.barMenuIcon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headingContainer}>
                        <Text style={styles.barText}>Farmbers</Text>
                    </View>
                    <View style={{ justifyContent: 'center', width }}>
                        <TouchableOpacity
                            style={styles.profileContainer}
                            onPress={() =>
                                this.props.navigation.navigate('Profile')}>
                            <Image source={Images.profilePic} style={styles.profileIcon} />
                        </TouchableOpacity>
                    </View>
                </View> */}
                 <View style={styles.upperBar}>
                <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
               
                <Image source={Images.backIcon} style={styles.backContainerStyle} />
                </TouchableOpacity>

                </View>
              <View style={styles.logoContainer}>
                 
                  <Image source={Images.farmLogo} style={styles.logoStyle} />
                 
               
              </View>
            
              <View style={{ position:'absolute', left:'70%',flexDirection:'row'}}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('NotificationScreen')}>
                  <Image source={Images.bell} style={styles.bellIcon1} />
                </TouchableOpacity>
            
              <View>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('Profile')}>
                  <Image source={Images.profilePic}  style={styles.profileIcon}/>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 8 }}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('ReferAndEarn')}>
                  <Image source={Images.refer} style={styles.bellIcon} />
                </TouchableOpacity>
              </View>
              </View>
            </View>
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                            <View style={styles.upperWrapper}>
                                <Text style={{ fontSize: 24, marginTop: 20, color: '#fff' }}>Enter Profile Info</Text>
                            </View>
                            <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', }}>
                                <TouchableOpacity onPress={this.selectImage}>
                                    <View style={styles.profileImgWrapper}>
                                        <Image source={Images.profilePic} style={{ height: 90, width: 90, borderRadius: 50, marginTop: -70, }} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ alignItems: 'center', }}>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                    style={styles.input} placeholder="Full Name"
                                />
                            </View>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                    style={styles.input} placeholder="Position"
                                />
                            </View>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                    style={styles.input} placeholder="Profession"
                                />
                            </View>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                    style={styles.input} placeholder="Adress"
                                />
                            </View>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                    style={styles.input} placeholder="Locality"
                                />
                            </View>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                    style={styles.input} placeholder="City"
                                />
                            </View>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                    style={styles.input} placeholder="State"
                                />
                            </View>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none'
                                    style={styles.input} placeholder="Pincode"
                                />
                            </View>
                            <View style={{ marginTop: 35 }}>
                                <Text style={{ paddingLeft: 10 }}>Short Profile / Bio</Text>
                                <View style={styles.formBox1}>
                                    <TextInput placeholderTextColor="#000" autoCapitalize='none' multiline={true} textAlignVertical='top'
                                        style={styles.input1} placeholder=""
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', top: height * 1.15 }}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.boxBtn2}
                            // onPress={() => this.props.navigation.navigate('PrivacyPolicyScreen')}
                            >
                                <Text style={styles.btnText}> DONE </Text>
                            </TouchableOpacity>
                        </View>
                        {/* <View style={styles.downWrapper}>
                        </View> */}
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
export default Profile;
