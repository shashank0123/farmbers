import React, { Component } from "react";
import { 
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,
    
    FlatList,
    Modal,
    I18nManager,
    RefreshControl,
    fetchData,
} from "react-native";
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils';
import styles from './style';


const Data= [
  {
      id: '1',
      name: 'Dinesh Pathak',
      message:' Hi...Its test Message',
      image: Images.profilePic
  },
  {
      id: '2',
      name: 'Dinesh Pathak',
      message:' Hi...Its test Message',
      image: Images.profilePic
  },
  {
    id: '3',
    name: 'Dinesh Pathak',
    message:' Hi...Its test Message',
    image: Images.profilePic
},
{
    id: '4',
    name: 'Dinesh Pathak',
    message:' Hi...Its test Message',
    image: Images.profilePic
},
{
    id: '5',
    name: 'Dinesh Pathak',
    message:' Hi...Its test Message',
    image: Images.profilePic
},
{
    id: '6',
    name: 'Dinesh Pathak',
    message:' Hi...Its test Message',
    image: Images.profilePic
},
 

];




class NotificationScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
            <View style={styles.upperBar}>
                <View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
               
                <Image source={Images.backIcon} style={styles.backContainerStyle} />
                </TouchableOpacity>

                </View>
              <View style={styles.logoContainer}>
                 
                  <Image source={Images.farmLogo} style={styles.logoStyle} />
                 
               
              </View>
            
              <View style={{ position:'absolute', left:'70%',flexDirection:'row'}}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('NotificationScreen')}>
                  <Image source={Images.bell} style={styles.bellIcon1} />
                </TouchableOpacity>
            
              <View>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('Profile')}>
                  <Image source={Images.profilePic}  style={styles.profileIcon}/>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 8 }}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('ReferAndEarn')}>
                  <Image source={Images.refer} style={styles.bellIcon} />
                </TouchableOpacity>
              </View>
              </View>
            </View>
            <View style={styles.wrapper}>
                    <View style={styles.upperWrapper}>
                        <Text style={{ fontSize: 24, marginTop: 10, color: '#fff' }}>Notification</Text>
                    </View>
                </View>
               
                <View style={{justifyContent:'center',alignItems:'center',}}>
                <View style={{ alignItems: 'center', marginBottom: '5%', }}>
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ marginTop: '10%', paddingBottom: '55%' }}
                            data={Data}
                            renderItem={({ item }) => (
                                <>
                                   
                                        <View style={styles.docDetailedWrapper2}>
                                            <View
                                                style={[styles.docSpecsWrapper, { marginTop: 10 }]}>
                                                      <Image source={Images.profilePic}  style={styles.profileIcon1}/>
                                                
                                                <View style={styles.docNameWrapper}>
                                                    <Text numberOfLines={1} style={styles.docNameText}>
                                                      {item.name}
                                                    </Text>
                                                    <Text numberOfLines={2} style={styles.docSubNameText}>
                                                       {item.message}
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                   
                                </>
                            )}
                            keyExtractor={item => item.id}
                        />
                    </View>


</View>

            </View>
        );
    }
}
export default NotificationScreen;


