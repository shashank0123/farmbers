import React, { Component } from "react";
import { 
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,
    
    FlatList,
    Modal,
    I18nManager,
    RefreshControl,
    fetchData,
} from "react-native";
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils';
import styles from './style';


const Data= [
  {
      id: '1',
      name: 'Lok Shabha',
      image: Images.parlmnt
  },
  {
      id: '2',
      name: 'Lok Shabha',
      image: Images.parlmnt
  },
 

];




class TermsAndConditions extends Component {
    render() {
        return (
            <View style={styles.container}>
            <View style={styles.upperBar}>
                <View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
               
                <Image source={Images.backIcon} style={styles.backContainerStyle} />
                </TouchableOpacity>

                </View>
              <View style={styles.logoContainer}>
                 
                  <Image source={Images.farmLogo} style={styles.logoStyle} />
                 
               
              </View>
            
              <View style={{ position:'absolute', left:'70%',flexDirection:'row'}}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('NotificationScreen')}>
                  <Image source={Images.bell} style={styles.bellIcon1} />
                </TouchableOpacity>
            
              <View>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('Profile')}>
                  <Image source={Images.profilePic}  style={styles.profileIcon}/>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 8 }}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('ReferAndEarn')}>
                  <Image source={Images.refer} style={styles.bellIcon} />
                </TouchableOpacity>
              </View>
              </View>
            </View>
            <View style={styles.wrapper}>
                    <View style={styles.upperWrapper}>
                        <Text style={{ fontSize: 24, marginTop: 10, color: '#fff' }}>Privacy Policy</Text>
                    </View>
                </View>
               
                <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View style={styles.wrapper}>
                              
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
                                <Text style={{ textAlign: 'left', fontSize: 18 }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release of
                                Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                                software like Aldus PageMaker including versions of Lorem Ipsum.
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                It has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release of
                                Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                            software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
                            </View>
                            {/* <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                <CheckBox
                                    style={{}}
                                    value={this.state.checked}
                                    tintColors={{ true: '#006631', false: '#A0A0A0' }}
                                    onValueChange={() => this.setState({ checked: !this.state.checked })}
                                />
                                <Text style={{ marginTop: 5, fontWeight: 'bold' }}> I Agree</Text>
                            </View> */}
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ position: 'absolute', top: 20, }}>
                                    <TouchableOpacity
                                        activeOpacity={1}
                                        style={styles.boxBtn2}
                                        onPress={() => this.props.navigation.navigate('MapScreen')}>
                                        <Text style={styles.btnText}> DONE </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                          
                        </ScrollView>
                    </KeyboardAvoidingView>

            </View>
        );
    }
}
export default TermsAndConditions;


