import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Modal,
  I18nManager,
  RefreshControl,
  fetchData,
  Alert
} from "react-native";
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils';
import styles from './style';
const Data = [
  {
    id: '1',
    name: 'Lok Shabha',
    image: Images.parlmnt
  },
  {
    id: '2',
    name: 'Lok Shabha',
    image: Images.parlmnt
  },
  {
    id: '3',
    name: 'Lok Shabha',
    image: Images.parlmnt
  },
];
class Lands extends Component {
    SampleFunction=()=>{
 
        // Alert.alert("Floating Button Clicked");
        this.props.navigation.navigate('AddLand')
    }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.backContainerStyle} />
            </TouchableOpacity>
          </View>
          <View style={styles.logoContainer}>
            <Image source={Images.farmLogo} style={styles.logoStyle} />
          </View>
          <View style={{ position: 'absolute', left: '70%', flexDirection: 'row' }}>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('NotificationScreen')}>
              <Image source={Images.bell} style={styles.bellIcon1} />
            </TouchableOpacity>
            <View>
              <TouchableOpacity onPress={() =>this.props.navigation.navigate('Profile')}>
                <Image source={Images.profilePic} style={styles.profileIcon} />
              </TouchableOpacity>
            </View>
            <View style={{ marginLeft: 8 }}>
              <TouchableOpacity onPress={() =>
                this.props.navigation.navigate('ReferAndEarn')}>
                <Image source={Images.refer} style={styles.bellIcon} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.wrapper}>
          <View style={styles.upperWrapper}>
            <Text style={{ fontSize: 24, marginTop: 10, color: '#fff' }}>Crops</Text>
          </View>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <FlatList
          
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ marginTop: '2%', paddingBottom: '55%' }}
            data={Data}
            renderItem={({ item }) => (
              <>
                <View style={styles.cardDetailedWrapper}>
                 
                    <View style={styles.docNameWrapper}>


                    <View style={{ flexDirection: 'row',backgroundColor:'#006631',alignItems:'center',paddingTop:10,paddingBottom:15,width:'100%',borderTopLeftRadius:10,borderTopRightRadius:10,paddingLeft:10}}>
                        <View style={{ flexDirection: 'column', width: '60%',}}>
                          <Text numberOfLines={1} style={{ fontSize: width * 0.045, paddingBottom: 1, fontWeight: 'bold', width: '80%',color:'#fff'  }}>Farm: NoidaFarm </Text>
                         
                        </View>
                        <View style={{ flexDirection: 'column', width: '50%', }}>
                          <Text numberOfLines={1} style={{ fontSize: width * 0.045, paddingBottom: 1, fontWeight: 'bold', width: '80%',color:'#fff'  }}>Village: Nurpur</Text>
                          
                        </View>
                      </View>

                      <View >
                      <Image source={Images.bhindi} style={{width:'100%',height:height*0.2}} />
                      </View>
                     
                     
                      <View style={{  flexDirection: 'row', marginTop:'2%',justifyContent:'space-between'}}>

                      <View style={{ flexDirection: 'column', marginTop:5,marginLeft:15}}>
                            <Text style={{}}>
                          <Text numberOfLines={1} style={{ fontSize: width * 0.045, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Crop :</Text>
                          <Text numberOfLines={1} style={{ fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', }}> Bhindi</Text>
                          </Text>
                            <TouchableOpacity style={styles.boxBtn} onPress={() =>this.props.navigation.navigate('CropListScreen')}>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.03, paddingBottom: 1,  color:'#000',}}>Know more </Text>
                        <Image source={Images.forward} style={styles.farwardIcon} />
                        </TouchableOpacity>
                        </View>
                      
                       
                       <View style={{ flexDirection: 'column', marginTop:5,marginRight:15}}>
                            <Text style={{}} >
                          <Text numberOfLines={1} style={{ fontSize: width * 0.045, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Soil Type :</Text>
                          <Text numberOfLines={1} style={{ fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', }}> Sand</Text>
                          </Text>
                            <TouchableOpacity style={styles.boxBtn1} onPress={() =>this.props.navigation.navigate('Soil')}>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.03, paddingBottom: 1,  color:'#000',}}>Know more </Text>
                        <Image source={Images.forward} style={styles.farwardIcon} />
                        </TouchableOpacity>
                        </View>
                      </View>
                    </View>
               
                </View>
              </>
            )}
            keyExtractor={item => item.id}
          />
        </View>
        {/* <View style={styles.MainContainer}> */}
 
 <TouchableOpacity activeOpacity={1} onPress={this.SampleFunction} style={styles.TouchableOpacityStyle} >

   <Image source={Images.add} 
   
          style={styles.FloatingButtonStyle} />
          {/* <Text>Add</Text> */}

 </TouchableOpacity>

{/* </View> */}
      </View>
    );
  }
}
export default Lands;
