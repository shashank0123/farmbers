import { combineReducers } from "redux"
import login from './login'
import profile from './profile'
const rootReducer = combineReducers({
  login: login,
  profile: profile,
})
export default rootReducer
