import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Modal,
  I18nManager,
  RefreshControl,
  fetchData,
} from "react-native";
const { width, height } = Dimensions.get('window');
import { Images } from '../../utils';
import styles from './style';
const Data = [
  {
    id: '1',
    time: 'Now',
    degree: '20',
    windspeed: '6.2km/h'
  },
  {
    id: '2',
    time: '4.40',
    degree: '25',
    windspeed: '7.2km/h'
  },
  {
    id: '3',
    time: '5.40',
    degree: '28',
    windspeed: '8.2km/h'
  },
  {
    id: '4',
    time: '6.40',
    degree: '22',
    windspeed: '9.2km/h'
  },
  {
    id: '5',
    time: '7.40',
    degree: '24',
    windspeed: '10.2km/h'
  },
  {
    id: '6',
    time: '8.40',
    degree: '30',
    windspeed: '18.2km/h'
  },
];
class Weather extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <View>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.backIcon} style={styles.backContainerStyle} />
            </TouchableOpacity>
          </View>
          <View style={styles.logoContainer}>
            <Image source={Images.farmLogo} style={styles.logoStyle} />
          </View>
          <View style={{ position: 'absolute', left: '70%', flexDirection: 'row' }}>
            <TouchableOpacity onPress={() =>
              this.props.navigation.navigate('NotificationScreen')}>
              <Image source={Images.bell} style={styles.bellIcon1} />
            </TouchableOpacity>
            <View>
              <TouchableOpacity onPress={() =>
                this.props.navigation.navigate('Profile')}>
                <Image source={Images.profilePic} style={styles.profileIcon} />
              </TouchableOpacity>
            </View>
            <View style={{ marginLeft: 8 }}>
              <TouchableOpacity onPress={() =>
                this.props.navigation.navigate('ReferAndEarn')}>
                <Image source={Images.refer} style={styles.bellIcon} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.wrapper}>
          <View style={styles.upperWrapper}>
            <Text style={{ fontSize: 20, marginTop: 10, color: '#fff' }}>Weather Information</Text>
          </View>
        </View>
        <KeyboardAvoidingView behavior="padding">
          <ScrollView
            contentContainerStyle={styles.scrollWrapper}
            showsVerticalScrollIndicator={false}>
            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
              <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                <Text style={{ fontSize: 45, fontWeight: 'bold', marginTop: '8%' }}> 18'C</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginTop: '1%' }}> Haze</Text>
              </View>
              <View style={styles.docDetailedWrapper1}>
                <View
                  style={[styles.docSpecsWrapper, { marginTop: 10 }]}>
                  <View style={styles.docNameWrapper}>
                    <View style={{ marginTop: 3, flexDirection: 'row', }}>
                      <Text numberOfLines={1} style={{ position: 'absolute', fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Today: Haze</Text>
                      <Text numberOfLines={1} style={{ position: 'absolute', fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', left: '75%', color: 'green' }}>18'/6'</Text>
                    </View>
                    <View style={{ marginTop: '12%', flexDirection: 'row', }}>
                      <Text numberOfLines={1} style={{ position: 'absolute', fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Tommarow: Haze</Text>
                      <Text numberOfLines={1} style={{ position: 'absolute', fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', left: '75%', color: 'green' }}>18'/6'</Text>
                    </View>
                    <View style={{ marginTop: '12%', flexDirection: 'row', }}>
                      <Text numberOfLines={1} style={{ position: 'absolute', fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Sun: Haze</Text>
                      <Text numberOfLines={1} style={{ position: 'absolute', fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', left: '75%', color: 'green' }}>18'/6'</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: height * 0.04 }}>
                <TouchableOpacity
                  activeOpacity={1}
                  style={styles.boxBtn2}
                  onPress={() => this.props.navigation.navigate('Dashboard')}>
                  <Text style={styles.btnText}> 5-Day Forcast </Text>
                </TouchableOpacity>
              </View>
              <FlatList style={{ marginTop: '10%', marginRight: '5%' }}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={Data}
                renderItem={({ item, index }) => (
                  <>
                    <View style={{
                      marginBottom: 10,
                      marginLeft: 35,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}>
                      <Text style={{ fontSize: 12, fontWeight: '500', paddingBottom: 5 }}>{item.time}</Text>
                      <Text style={{ fontSize: 14, fontWeight: 'bold', paddingBottom: 10 }}>{item.degree}' C</Text>
                      <Image source={Images.sun} style={styles.sunIcon} />
                      <Text style={{ fontSize: 10, fontWeight: 'bold', paddingBottom: 10, marginTop: 10 }}>{item.windspeed}</Text>
                    </View>
                  </>
                )}
                keyExtractor={item => item.id}
              />
              <View style={styles.docDetailedWrapper1}>
                <View
                  style={[styles.docSpecsWrapper, { marginTop: 20 }]}>
                  <View style={styles.docNameWrapper}>
                    <View style={{ marginTop: 3, flexDirection: 'row', }}>
                      <View style={{ flexDirection: 'column', width: '50%' }}>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.035, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Real feel:</Text>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', }}> 16'C</Text>
                      </View>
                      <View style={{ flexDirection: 'column', width: '50%', marginLeft: '20%' }}>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.035, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Humidity:</Text>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', }}> 59%</Text>
                      </View>
                    </View>
                    <View style={{ marginTop: '12%', flexDirection: 'row', }}>
                      <View style={{ flexDirection: 'column', width: '50%' }}>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.035, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Wind Speed:</Text>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', }}>8km/h</Text>
                      </View>
                      <View style={{ flexDirection: 'column', width: '50%', marginLeft: '20%' }}>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.035, paddingBottom: 1, fontWeight: 'bold', width: '80%', color: 'green' }}>Chance of rain:</Text>
                        <Text numberOfLines={1} style={{ fontSize: width * 0.04, paddingBottom: 1, fontWeight: 'bold', width: '80%', }}> 59%</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
export default Weather;
