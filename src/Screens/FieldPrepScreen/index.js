import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Picker, FlatList
} from "react-native";
const { width, height } = Dimensions.get('window');
import CheckBox from '@react-native-community/checkbox';
import DateTimePicker from '@react-native-community/datetimepicker';
import InputField from '../../Commons/input';
import { Images } from '../../utils'
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import AudioRecord from 'react-native-audio-record';


class FieldPrepScreen extends Component {
   
    
    state = {
        audioFile: '',
        recording: false,
        loaded: false,
        paused: true
      };

      async componentDidMount() {
        await this.checkPermission();
    
        const options = {
          sampleRate: 16000,
          channels: 1,
          bitsPerSample: 16,
          wavFile: 'test.wav'
        };
    
        AudioRecord.init(options);
    
        AudioRecord.on('data', data => {
          const chunk = Buffer.from(data, 'base64');
          console.log('chunk size', chunk.byteLength);
          // do something with audio chunk
        });
      }
    //   checkPermission = async () => {
    //     const p = await Permissions.check('microphone');
    //     console.log('permission check', p);
    //     if (p === 'authorized') return;
    //     return this.requestPermission();
    //   };
    //   requestPermission = async () => {
    //     const p = await Permissions.request('microphone');
    //     console.log('permission request', p);
    //   };
    
      start = () => {
        console.log('start record');
        this.setState({ audioFile: '', recording: true, loaded: false });
        AudioRecord.start();
      };
    
    //   stop = async () => {
    //     if (!this.state.recording) return;
    //     console.log('stop record');
    //     let audioFile = await AudioRecord.stop();
    //     console.log('audioFile', audioFile);
    //     this.setState({ audioFile, recording: false });
    //   };
    //   load = () => {
    //     return new Promise((resolve, reject) => {
    //       if (!this.state.audioFile) {
    //         return reject('file path is empty');
    //       }
    
    //       this.sound = new Sound(this.state.audioFile, '', error => {
    //         if (error) {
    //           console.log('failed to load the file', error);
    //           return reject(error);
    //         }
    //         this.setState({ loaded: true });
    //         return resolve();
    //       });
    //     });
    //   };


//   play = async () => {
//     if (!this.state.loaded) {
//       try {
//         await this.load();
//       } catch (error) {
//         console.log(error);
//       }
//     }

//     this.setState({ paused: false });
//     Sound.setCategory('Playback');

//     this.sound.play(success => {
//       if (success) {
//         console.log('successfully finished playing');
//       } else {
//         console.log('playback failed due to audio decoding errors');
//       }
//       this.setState({ paused: true });
//       // this.sound.release();
//     });
//   };

//   pause = () => {
//     this.sound.pause();
//     this.setState({ paused: true });
//   };

    
   
   
    render() {
        const { recording, paused, audioFile } = this.state;
        return (
            <View style={styles.container}>
               
                 <View style={styles.upperBar}>
                <View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
               
                <Image source={Images.backIcon} style={styles.backContainerStyle} />
                </TouchableOpacity>

                </View>
              <View style={styles.logoContainer}>
                 
                  <Image source={Images.farmLogo} style={styles.logoStyle} />
                 
               
              </View>
            
              <View style={{ position:'absolute', left:'70%',flexDirection:'row'}}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('NotificationScreen')}>
                  <Image source={Images.bell} style={styles.bellIcon1} />
                </TouchableOpacity>
            
              <View>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('Profile')}>
                  <Image source={Images.profilePic}  style={styles.profileIcon}/>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 8 }}>
                <TouchableOpacity onPress={() =>
                  this.props.navigation.navigate('ReferAndEarn')}>
                  <Image source={Images.refer} style={styles.bellIcon} />
                </TouchableOpacity>
              </View>
              </View>
            </View>
                {/* <ScrollView
                    // contentContainerStyle={styles.scrollWrapper}
                    showsVerticalScrollIndicator={false}> */}
                    <View style={styles.scrollWrapper}>
                        <View style={styles.docDetailedWrapper2}>
                            <View style={styles.middleWrapper}>
                                <View style={{ width: '100%', height: '33%', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.nameWrapper} >Need Help ?</Text>
                                </View>
                               
                            </View>
                            <View style={{ justifyContent: 'flex-start', alignItems:'flex-start', margin: 10, }}>
                           
                            <Text style={{fontSize:16,fontWeight:'bold'}}>Enter Your Query :</Text>
                            <TextInput

                             placeholderTextColor="#000"
                              autoCapitalize='none'
                               multiline={true} 
                               maxHeight={120}
                               textAlignVertical='top'
                              style={styles.input1}
                              underlineColorAndroid='#000'
                               placeholder="Write your Problem"
                                />
                      
                            </View>
                        </View>
                        <View style={styles.bottomBar}>
                            <TouchableOpacity >
                                <Image source={Images.call} style={styles.searchIcon} />
                            </TouchableOpacity>
                            <TouchableOpacity  onPress={this.start} title="Record" >
                                <Image source={Images.mic} style={styles.searchIcon} />
                            </TouchableOpacity>
                            <TouchableOpacity >
                                <Image source={Images.sms} style={styles.searchIcon} />
                            </TouchableOpacity>
                        </View>
                        {/* <View style={styles.downWrapper}>
                        </View> */}
                    </View>
                {/* </ScrollView> */}
            </View>
        );
    }
}
export default FieldPrepScreen;
